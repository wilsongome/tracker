package br.com.ok.tracker.activity;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.concurrent.ExecutionException;

import br.com.ok.tracker.R;
import br.com.ok.tracker.adapter.TabsAdapter;
import br.com.ok.tracker.helper.Preferencias;
import br.com.ok.tracker.lib.Traccar;
import br.com.ok.tracker.util.SlidingTabLayout;

public class MainActivity extends AppCompatActivity{

    private Toolbar toolbarPrincipal;
    private SlidingTabLayout slidingTabLayout;
    private ViewPager viewPager;
    private GoogleMap mMap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Configura o mapa
        // Add a marker in Sydney and move the camera


        //LatLng sydney = new LatLng(-34, 151);
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        //Configura toolbar
        toolbarPrincipal = (Toolbar) findViewById(R.id.toolbar_principal);
        toolbarPrincipal.setLogo( R.drawable.logo_header );
        setSupportActionBar( toolbarPrincipal );

        //Configura abas
        slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tab_main);
        viewPager = (ViewPager) findViewById(R.id.view_pager_main);

        //configurar adapter
        TabsAdapter tabsAdapter = new TabsAdapter( getSupportFragmentManager(), this );
        viewPager.setAdapter( tabsAdapter );
        slidingTabLayout.setCustomTabView(R.layout.tab_view, R.id.text_item_tab);
        slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setSelectedIndicatorColors( ContextCompat.getColor(this, R.color.colorPrimaryDark) );
        slidingTabLayout.setViewPager( viewPager );

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_exit:
                deslogarUsuario();
                return true;
            case R.id.action_settings:
                getMap();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void deslogarUsuario(){
        Preferencias preferencias = new Preferencias(this);
        preferencias.setCookie("");
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void getMap(){
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }



}
