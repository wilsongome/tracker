package br.com.ok.tracker.util;

import android.os.AsyncTask;
import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by wilson on 27/04/18.
 */

public class HttpRequestView extends AsyncTask <String, Void, String>{


    public void Executar(String dados) throws IOException {
        String host = "http://rastreamento.oktor.com.br/api/session/";
        host = host + dados;
        // Create URL
        URL url = new URL(host);

        // Create connection
        HttpsURLConnection myConnection =(HttpsURLConnection) url.openConnection();
        myConnection.setRequestProperty("Content-type", "application/x-www-form-urlencoded");

        if (myConnection.getResponseCode() == 200) {
            InputStream responseBody = myConnection.getInputStream();
            InputStreamReader responseBodyReader = new InputStreamReader(responseBody, "UTF-8");
            JsonReader jsonReader = new JsonReader(responseBodyReader);

            jsonReader.close();
            myConnection.disconnect();

        } else {
            // Error handling code goes here
        }
    }


    @Override
    protected String doInBackground(String... strings) {
        try {
            String url = "email=wilsongome@gmail.com&password=@w331400";
            this.Executar(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
