package br.com.ok.tracker.lib;
import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import br.com.ok.tracker.helper.Preferencias;


/**
 * Created by wilson on 27/04/18.
 */

public class Traccar extends AsyncTask <String, Void, String>{

    private String baseUrl = "http://rastreamento.oktor.com.br";
    private String contentType = "application/x-www-form-urlencoded";
    private String method = "POST";
    private Context context;
    private String action;
    String result = null;
    private String cookie = null;

    public Traccar(Context c,  String action){
        this.context = c;
        this.action = action;

    }

    @Override
    protected String doInBackground(String... params) {
        switch (this.action){
            case "login":
               this.LoginUsuario(params[0],params[1]);
                break;
            case "devices":
                this.getDevices();
                break;
            case "position":
                this.getPosition(params[0]);
                break;
        }
        return this.result;
    }



    private void LoginUsuario(String email, String senha){
        String action = "/api/session/";
        HashMap<String, String> values = new HashMap<String, String>();
        values.put("password",senha);
        values.put("email",email);
        this.HttpRequest(action, values);
    }

    private void getDevices(){
        String action = "/api/devices/";
        this.method = "GET";
        this.contentType = "application/json";
        HashMap<String, String> values = new HashMap<String, String>();
        this.HttpRequest(action, values);
    }
    private void getPosition(String id){
        String action = "/api/positions/";
        this.method = "GET";
        this.contentType = "application/json";
        HashMap<String, String> values = new HashMap<String, String>();
        values.put("id",id);
        this.HttpRequest(action, values);
    }


    private void HttpRequest(String action, HashMap<String, String> values){
        // Create URL
        URL url = null;
        try {
            url = new URL(this.baseUrl+action);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        // Create connection
        try {
            Map<String,Object> params = new LinkedHashMap<>();
            Set set = values.entrySet();
            Iterator itr = set.iterator();
            boolean hasParameters = false;
            while(itr.hasNext()) {
                Map.Entry mentry = (Map.Entry)itr.next();
                String key = (String) mentry.getKey().toString();
                String valor = (String) mentry.getValue().toString();
                params.put(key, valor);
                hasParameters = true;
            }

            StringBuilder postData = new StringBuilder();
            for (Map.Entry<String,Object> param : params.entrySet()) {
                if (postData.length() != 0) postData.append('&');
                postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                postData.append('=');
                postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
            }
            byte[] postDataBytes = postData.toString().getBytes("UTF-8");

            //Verifica se ja existe cookie setado
            Preferencias preferencias = new Preferencias(this.context);
            String cookie = preferencias.getCookie();

            HttpURLConnection myConnection = (HttpURLConnection) url.openConnection();
            myConnection.setRequestProperty("Content-Type",this.contentType);
            if(cookie!=null && cookie!="") {
                myConnection.setRequestProperty("Cookie", cookie);
            }
            myConnection.setRequestMethod(this.method);
            if(hasParameters) {
                myConnection.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                myConnection.setDoOutput(true);
                myConnection.getOutputStream().write(postDataBytes);
            }


            Integer response_code = myConnection.getResponseCode();

            if (response_code == 200) {
                InputStream responseBody = myConnection.getInputStream();
                String r = getStringFromInputStream(responseBody);
                this.result = r;

                if(cookie==null || cookie=="") {
                    //Obtem o cookie
                    String cookieHeader = myConnection.getHeaderField(1);
                    String[] cookiePartes = cookieHeader.split(";");
                    this.cookie = cookiePartes[0];
                    preferencias.setCookie(this.cookie);
                }

            } else {
                this.result = "erro";
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private void HttpRequest2(String action, HashMap<String, String> values){
        // Create URL
        URL url = null;
        try {
            url = new URL(this.baseUrl+action);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        // Create connection
        try {
            Map<String,Object> params = new LinkedHashMap<>();
            Set set = values.entrySet();
            Iterator itr = set.iterator();


            while(itr.hasNext()) {
                Map.Entry mentry = (Map.Entry)itr.next();
                String key = (String) mentry.getKey().toString();
                String valor = (String) mentry.getValue().toString();
                params.put(key, valor);
            }

            StringBuilder postData = new StringBuilder();
            for (Map.Entry<String,Object> param : params.entrySet()) {
                if (postData.length() != 0) postData.append('&');
                postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                postData.append('=');
                postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
            }
            byte[] postDataBytes = postData.toString().getBytes("UTF-8");


            //Verifica se ja existe cookie setado
            Preferencias preferencias = new Preferencias(this.context);
            String cookie = preferencias.getCookie();

            HttpURLConnection myConnection = (HttpURLConnection) url.openConnection();
            myConnection.setRequestProperty("Content-Type",this.contentType);
            if(!cookie.isEmpty()) {
                myConnection.setRequestProperty("Cookie", cookie);
            }
            myConnection.setRequestMethod(this.method);
            myConnection.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
            //myConnection.setDoOutput(true);
            //myConnection.getOutputStream().write(postDataBytes);


            Integer response_code = myConnection.getResponseCode();

            if (response_code == 200) {
                InputStream responseBody = myConnection.getInputStream();
                String r = getStringFromInputStream(responseBody);
                this.result = r;

                if(cookie.isEmpty()) {
                    //Obtem o cookie
                    String cookieHeader = myConnection.getHeaderField(1);
                    String[] cookiePartes = cookieHeader.split(";");
                    this.cookie = cookiePartes[0];
                    preferencias.setCookie(this.cookie);
                }
                //JSONObject mainObject = new JSONObject(r);
                //email = mainObject.getString("email");


            } else {
                this.result = "erro";
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }



    // convert InputStream to String
    private static String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }




}
