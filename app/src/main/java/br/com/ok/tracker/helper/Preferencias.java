package br.com.ok.tracker.helper;
import android.content.Context;
import android.content.SharedPreferences;


public class Preferencias {

    private Context contexto;
    private SharedPreferences preferences;
    private final String NOME_ARQUIVO = "tracker.preferencias";
    private final int MODE = 0;
    private SharedPreferences.Editor editor;

    private final String CHAVE_COOKIE = "cookie";
    private final String CHAVE_EMAIL = "email";
    private final String CHAVE_SENHA = "senha";

    public Preferencias( Context contextoParametro){
        contexto = contextoParametro;
        preferences = contexto.getSharedPreferences(NOME_ARQUIVO, MODE );
        editor = preferences.edit();
    }

    public void salvarDados( String email, String senha){

        editor.putString(CHAVE_EMAIL, email);
        editor.putString(CHAVE_SENHA, senha);
        editor.commit();

    }

    public void setCookie( String cookie){
        editor.putString(CHAVE_COOKIE, cookie);
        editor.commit();
    }

    public String getCookie(){
        if(preferences!=null){
            return preferences.getString(CHAVE_COOKIE, null);
        }else{
            return null;
        }

    }

    /*
    public String getNome(){
        if(preferences !=null){
            return preferences.getString(CHAVE_NOME, null);
        }else{
            return null;
        }
    }
*/

    public String getEmail(){
        if(preferences !=null){
            return preferences.getString(CHAVE_EMAIL, null);
        }else{
            return null;
        }
    }

    public String getSenha(){
        if(preferences !=null){
            return preferences.getString(CHAVE_SENHA, null);
        }else{
            return null;
        }
    }

}
