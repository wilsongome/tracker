package br.com.ok.tracker.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.ok.tracker.R;
import br.com.ok.tracker.lib.Traccar;
import br.com.ok.tracker.models.Devices;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;

    public MapFragment() {
        // Required empty public constructor
    }



    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_map, container, false);

        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            this.updatePosition();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void updatePosition() throws JSONException {
        mMap = googleMap;

        String resultDevices = new Traccar(getActivity(), "devices").execute("").get();
        JSONArray jsonArray = new JSONArray(resultDevices);
        for (int i=0; i < jsonArray.length(); i++) {

            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Devices modelDevices = new Devices();
            modelDevices.ExchangeJson(jsonObject);
            String idLastPostion = Integer.toString(modelDevices.getPositionId());
            String resultPosition = new Traccar(getActivity(),"position").execute(idLastPostion).get();

            // Add a marker in Sydney and move the camera
            LatLng sydney = new LatLng(-23.533773, -46.625290);
            mMap.addMarker(new MarkerOptions().position(sydney).title(modelDevices.getName()));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
            mMap.moveCamera(CameraUpdateFactory.zoomTo(10));

        }
    }
}
