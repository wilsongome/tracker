package br.com.ok.tracker.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by wilson on 02/05/18.
 */

public class Devices {
    private int id;
    private String name;
    private String uniqueId;
    private String status;
    private String lastUpdate;
    private int positionId;
    private int groupId;
    private String phone;
    private String model;
    private String contact;
    private String category;

    public void ExchangeJson(JSONObject json) throws JSONException {
        this.setId(json.getInt("id"));
        this.setName(json.getString("name"));
        this.setUniqueId(json.getString("uniqueId"));
        this.setStatus(json.getString("status"));
        this.setLastUpdate(json.getString("lastUpdate"));
        this.setPositionId(json.getInt("positionId"));
        this.setGroupId(json.getInt("groupId"));
        this.setPhone(json.getString("phone"));
        this.setModel(json.getString("model"));
        this.setContact(json.getString("contact"));
        this.setCategory(json.getString("category"));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public int getPositionId() {
        return positionId;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
