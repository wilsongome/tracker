package br.com.ok.tracker.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ImageSpan;

import br.com.ok.tracker.R;
import br.com.ok.tracker.fragments.AlertsFragment;
import br.com.ok.tracker.fragments.DevicesFragment;
import br.com.ok.tracker.fragments.MapFragment;

/**
 * Created by wilson on 25/04/18.
 */

public class TabsAdapter extends FragmentStatePagerAdapter {
    private Context context;
    private String[] abas = new String[]{"MAPA","DEVICES","ALERTAS"};
    private int[] icones = new int[]{R.drawable.ic_my_location,R.drawable.ic_local_taxi,R.drawable.ic_error_outline};
    private int tamanho_icone;

    public TabsAdapter(FragmentManager fm, Context c) {
        super(fm);
        this.context = c;
        double escala = this.context.getResources().getDisplayMetrics().density;
        tamanho_icone = (int) (27 * escala);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;

        switch (position){
            case 0:
                fragment = new MapFragment();
                break;
            case 1:
                fragment = new DevicesFragment();
                break;
            case 2:
                fragment = new AlertsFragment();
                break;
        }

        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Drawable drawable = ContextCompat.getDrawable(this.context, icones[position]);
        drawable.setBounds(0,0,tamanho_icone,tamanho_icone);
        ImageSpan imageSpan = new ImageSpan(drawable);
        SpannableString spannableString = new SpannableString(" ");
        spannableString.setSpan(imageSpan, 0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        //return abas[position];
        return spannableString;
    }

    @Override
    public int getCount() {
        return abas.length;
    }
}
