package br.com.ok.tracker.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import br.com.ok.tracker.R;
import br.com.ok.tracker.models.Devices;


public class DevicesAdapter extends ArrayAdapter<Devices> {

    private ArrayList<Devices> devices;
    private Context context;

    public DevicesAdapter(Context c, ArrayList<Devices> objects) {
        super(c, 0, objects);
        this.context = c;
        this.devices = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = null;

        // Verifica se a lista está preenchida
        if( devices != null ){

            // inicializar objeto para montagem da view
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

            // Monta view a partir do xml
            view = inflater.inflate(R.layout.device_item_lista, parent, false);

            // recupera elemento para exibição
            TextView nome = (TextView) view.findViewById(R.id.tv_titulo);
            TextView subtile = (TextView) view.findViewById(R.id.tv_subtitulo);
            TextView last_sync = (TextView) view.findViewById(R.id.tv_date_sync);

            Devices device = devices.get(position);
            String data = null;
            if(!device.getLastUpdate().isEmpty()){
                String[] data_partes = device.getLastUpdate().split("T");
                String dt = data_partes[0];
                String time = data_partes[1].substring(0,8);
                data = dt + " " + time;
            }

            nome.setText( device.getName() );
            subtile.setText( device.getUniqueId() );
            last_sync.setText( data );


        }

        return view;
    }
}

