package br.com.ok.tracker.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import br.com.ok.tracker.R;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }
}
