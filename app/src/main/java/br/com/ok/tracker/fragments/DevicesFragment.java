package br.com.ok.tracker.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;

import br.com.ok.tracker.R;
import br.com.ok.tracker.adapter.DevicesAdapter;
import br.com.ok.tracker.lib.Traccar;
import br.com.ok.tracker.models.Devices;

/**
 * A simple {@link Fragment} subclass.
 */
public class DevicesFragment extends Fragment {

    private ListView listView;
    private ArrayAdapter<Devices> adapter;
    private ArrayList<Devices> devices;

    public DevicesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_devices, container, false);
        //Inicia o arrayList
        devices = new ArrayList<>();
        listView = (ListView) view.findViewById(R.id.lv_devices);
        adapter = new DevicesAdapter(getActivity(),devices);
        listView.setAdapter( adapter );

        try {
            this.getDevices();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return view;
    }


    public void getDevices() throws ExecutionException, InterruptedException, JSONException {

        String result = new Traccar(getActivity(), "devices").execute("").get();
        JSONArray jsonArray = new JSONArray(result);
        devices.clear();
        for (int i=0; i < jsonArray.length(); i++) {

            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Devices modelDevices = new Devices();
            modelDevices.ExchangeJson(jsonObject);
            devices.add(modelDevices);
        }

        adapter.notifyDataSetChanged();
    }




}
