package br.com.ok.tracker.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.JsonReader;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import javax.net.ssl.HttpsURLConnection;

import br.com.ok.tracker.R;
import br.com.ok.tracker.helper.Preferencias;
import br.com.ok.tracker.lib.Traccar;

public class LoginActivity extends AppCompatActivity {

    Button btnLogar;
    EditText txtUsuario;
    EditText txtSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        btnLogar = (Button) findViewById(R.id.btnLogar);
        txtUsuario = (EditText) findViewById(R.id.txtUsuario);
        txtSenha = (EditText) findViewById(R.id.txtSenha);

        //Carrega os dados de acesso
        Preferencias preferencias = new Preferencias(this);
        txtUsuario.setText(preferencias.getEmail());
        txtSenha.setText(preferencias.getSenha());

        btnLogar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    autenticarUsuario();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void autenticarUsuario() throws ExecutionException, InterruptedException {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setTitle("Autenticando...");
        dialog.setMessage("Por favor aguarde");
        String usuario = this.txtUsuario.getText().toString();
        String senha = this.txtSenha.getText().toString();
        if(!(usuario.isEmpty() && senha.isEmpty())) {
            dialog.show();
            String result = new Traccar(getBaseContext(), "login").execute(usuario, senha).get();
            if(result !="erro"){
                //Se autenticou, salva os dados pra facilitar uma nova autenticaçao
                Preferencias preferencias = new Preferencias(this);
                preferencias.salvarDados(usuario, senha);

                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
            }else{
                dialog.dismiss();
                Toast toast = Toast.makeText(this, "Erro! Nao foi possivel autenticar.",Toast.LENGTH_SHORT);
                toast.show();
            }

        }else{
            Toast toast = Toast.makeText(this, "Digite os dados corretamente!",Toast.LENGTH_SHORT);
            toast.show();
        }
    }




}
